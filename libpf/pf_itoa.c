#include "../includes/libpf.h"

char	*pf_itoa(int n)
{
	char			*ret;
	unsigned int	size;
	unsigned int	tmp;

	size = (n < 0) ? 2 : 1;
	tmp = (n < 0) ? -n : n;
	while (tmp >= 10 && (tmp /= 10))
		++size;
	if (!(ret = malloc(sizeof(char) * size + 1)))
		return (NULL);
	if (n < 0)
		ret[0] = '-';
	ret[size] = '\0';
	tmp = (n < 0) ? -n : n;
	while (tmp >= 10)
	{
		ret[--size] = (tmp % 10) + '0';
		tmp /= 10;
	}
	ret[--size] = (tmp % 10) + '0';
	return (ret);
}
