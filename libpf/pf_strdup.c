#include "../includes/libpf.h"

char	*pf_strdup(const char *s1)
{
	int		i;
	int		size;
	char	*dst;

	i = 0;
	size = pf_strlen(s1);
	if (!(dst = malloc(sizeof(char) * size + 1)))
		return (NULL);
	while (s1[i])
	{
		dst[i] = s1[i];
		i++;
	}
	dst[i] = '\0';
	return (dst);
}
